
Summary
-------
This module allows for your Drupal site to connect to http://yr.no and gather weather forecast data to display in a page view and one or several blocks on your site.
Two template files and a stylesheet is included for theming. Note that you should keep the link back to http://yr.no intact if you override these. 

It is highly recommended to install the jquery_ui module. This will progressively enhance the forecast-page for users with javascript enabled.
For Drupal 7, jQuery UI is included in core.


Requirements
------------
PHP 5 and cURL.


Installation
------------
Install the module as usual, see http://drupal.org/node/70151 for further information.

7.x-1.x and 6.x-2.x branches:
For the added functionality of an autocomplete field for adding locations, you must activate that option in the settings for yr_verdata, and then get the files with all the locations from http://yr.freka.net and import them into the database. These files are adapted from http://yr.no (international file originally adapted by the fine people at yr.no from geonames.org) and released under the Creative Commons Attribution 3.0 License.


Configuration and usage
-----------------------
* Configure the module at admin/settings/yr_verdata (D6) or admin/config/system/yr_verdata (D7).
* Add and delete locations at admin/build/yr_verdata (D6) or admin/structure/yr_verdata (D7).
* A list of locations can be seen at /yr_verdata, and in a block (or several blocks if you activate the multiblock feature) that is available at admin/build/blocks (D6) or admin/structure/block (D7).


Author
------
Fredrik Kilander (tjodolv, http://drupal.org/user/196733/contact)


Legal
-----
All forecast data are collected from http://yr.no, which is operated by the Norwegian Meteorological Institute and Norwegian Broadcasting Corporation. All data is free as in free beer, as long as a link back to http://yr.no is provided.
See http://www.yr.no/english/1.2025949.