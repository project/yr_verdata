<?php

/**
 * @file
 * This file contains the functions for the admin interface for yr_verdata.
 */

/**
 * Administrative settings for yr_verdata.
 *
 * @return
 * Returns a form array to be processed by drupal_get_form().
 */
function yr_verdata_settings(&$form_state) {
  drupal_add_js(drupal_get_path('module', 'yr_verdata') . '/js/yr_verdata.admin.js');
  $form = array();

  $form['yc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forecast customization'),
    '#description' => t('Choose the default formats and unit types you want to display the forecasts in.'),
  );
  $available_date_types = system_get_date_types();
  $date_options = array();
  foreach ($available_date_types as $type) {
    $date_options[$type['type']] = $type['title'];
  }
  $form['yc']['yr_verdata_date_format'] = array(
    '#type' => 'select',
    '#title' => t('Date format'),
    '#options' => $date_options,
    '#default_value' => variable_get('yr_verdata_date_format', 'medium'),
    '#description' => t('These formats can be managed at !datetime.', array('!datetime' => l('admin/config/regional/date-time', 'admin/config/regional/date-time'))),
  );
  $form['yc']['yr_verdata_temp_unit'] = array(
    '#type' => 'select',
    '#title' => t('Temperature unit'),
    '#options' => array('celcius' => t('Celcius'), 'fahrenheit' => t('Fahrenheit'), 'kelvin' => t('Kelvin')),
    '#default_value' => variable_get('yr_verdata_temp_unit', 'celcius'),
  );
  $form['yc']['yr_verdata_press_unit'] = array(
    '#type' => 'select',
    '#title' => t('Pressure unit'),
    '#options' => drupal_map_assoc(array('hPa', 'bar', 'psi', 'inHg', 'torr')),
    '#default_value' => variable_get('yr_verdata_press_unit', 'hPa'),
  );
  $form['yc']['yr_verdata_precip_unit'] = array(
    '#type' => 'select',
    '#title' => t('Precipitation unit'),
    '#options' => drupal_map_assoc(array('mm', 'in')),
    '#default_value' => variable_get('yr_verdata_precip_unit', 'mm'),
  );
  $form['yc']['yr_verdata_windspeed_unit'] = array(
    '#type' => 'select',
    '#title' => t('Wind speed unit'),
    '#options' => array('mps' => t('Meters per second'), 'knots' => t('Knots'), 'kph' => t('Kilometers per hour'), 'mph' => t('Miles per hour')),
    '#default_value' => variable_get('yr_verdata_windspeed_unit', 'mps'),
  );
  $form['yc']['yr_verdata_name_display'] = array(
    '#type' => 'select',
    '#title' => t('Location name format'),
    '#options' => array('1' => t('Location'), '2' => t('Location, Region'), '3' => t('Location, Country'), '4' => t('Location, Region, Country')),
    '#default_value' => variable_get('yr_verdata_name_display', '1'),
    '#description' => t('How the location name should be output in page titles and listings.'),
  );

  $form['yr_verdata_maxage'] = array(
    '#type' => 'select',
    '#title' => t('Forecast maxage'),
    '#options' => array(0 => t('Always update (development only!)'), 3600 => t('One hour'), 10800 => t('Three hours'), 21600 => t('Six hours (recommended)'), 43200 => t('12 hours'), 86800 => t('24 hours')),
    '#default_value' => variable_get('yr_verdata_maxage', 21600),
    '#description' => t('The longest time that will pass before the module attempts to update the locally stored forecast with a new one from yr.no. Note that yr.no does not update most locations more often than every 5-6 hours. Yr.no will block ip-addresses that request forecasts too often.'),
  );

  $form['yr_verdata_multiblocks'] = array(
    '#type' => 'radios',
    '#title' => t('Multiple blocks'),
    '#options' => array('on' => t('On'), 'off' => t('Off')),
    '#default_value' => variable_get('yr_verdata_multiblocks', 'off'),
    '#description' => t('Yr verdata provides one block, listing all locations. If you want to, it can additionally provide one block for each location. Note that this may look cluttered, so it can be a good idea to use page-specific settings for these blocks.'),
  );

  $form['yr_verdata_debug'] = array(
    '#type' => 'radios',
    '#title' => t('Raw forecast'),
    '#options' => array('on' => t('On'), 'off' => t('Off')),
    '#description' => t('Setting this to "on" will make the full, unprocessed xml-feed from yr.no available to the theme layer. Note that this is not considered output-safe, and must be sanitized before use in a template.'),
    '#default_value' => variable_get('yr_verdata_debug', 'off'),
  );

  $form = system_settings_form($form);
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults'));
  unset($form['#submit']);
  $form['#submit'][] = 'yr_verdata_settings_submit';
  return $form;
}

/**
 * Submit handler for yr_verdata_settings().
 */
function yr_verdata_settings_submit($form, &$form_state) {
  // If the user hit the 'Reset to defaults button' we do that then return.
  if ($form_state['clicked_button']['#parents'][0] == 'reset') {
    variable_set('yr_verdata_date_format', 'medium');
    variable_set('yr_verdata_temp_unit', 'celcius');
    variable_set('yr_verdata_press_unit', 'hPa');
    variable_set('yr_verdata_precip_unit', 'mm');
    variable_set('yr_verdata_windspeed_unit', 'mps');
    variable_set('yr_verdata_name_display', '1');
    variable_set('yr_verdata_maxage', 21600);
    variable_set('yr_verdata_debug', 'off');
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    variable_set('yr_verdata_date_format', $form_state['values']['yr_verdata_date_format']);
    variable_set('yr_verdata_temp_unit', $form_state['values']['yr_verdata_temp_unit']);
    variable_set('yr_verdata_press_unit', $form_state['values']['yr_verdata_press_unit']);
    variable_set('yr_verdata_precip_unit', $form_state['values']['yr_verdata_precip_unit']);
    variable_set('yr_verdata_windspeed_unit', $form_state['values']['yr_verdata_windspeed_unit']);
    variable_set('yr_verdata_name_display', $form_state['values']['yr_verdata_name_display']);
    variable_set('yr_verdata_maxage', $form_state['values']['yr_verdata_maxage']);
    variable_set('yr_verdata_multiblocks', $form_state['values']['yr_verdata_multiblocks']);
    variable_set('yr_verdata_debug', $form_state['values']['yr_verdata_debug']);
    drupal_set_message(t('Settings saved.'));
  }
}

/**
 * Function for adding locations to the database table.
 */
function yr_verdata_add_form() {
  $form['yvo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add location by URL'),
  );
  $form['yvo']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The base URL to the forecast for this location. For example !example_url.', array('!example_url' => l('http://www.yr.no/sted/Norge/Vest-Agder/Kristiansand/Kristiansand/', 'http://www.yr.no/sted/Norge/Vest-Agder/Kristiansand/Kristiansand/'))),
    '#size' => 60,
    '#required' => TRUE,
  );
  $form['yvo']['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9)),
    '#default_value' => 5,
    '#description' => t('Lower values will be displayed first in listings, tables and blocks.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add location'),
  );
  drupal_set_breadcrumb(array(l(t('Home'), '<front>'), l(t('Forecast'), 'forecast')));
  return $form;
}

/**
 * Validation handler for yr_verdata_add_form().
 */
function yr_verdata_add_form_validate($form, &$form_state) {
  // Verify that the url entered is actually for yr.no.
  $start = drupal_substr($form_state['values']['url'], 0, 17);
  if ($start !== 'http://www.yr.no/') {
    form_set_error('url', t('The url must be a valid yr.no address.'));
  }
  // Check if the location already exists.
  $result = db_query("SELECT yid FROM {yr_verdata} WHERE url = :url", array(
    ':url' => $form_state['values']['url'],
  ));
  if ($result->fetch() == TRUE) {
    form_set_error('url', t('The location already exists in the database.'));
  }
}

/**
 * Submit handler for yr_verdata_add_form().
 */
function yr_verdata_add_form_submit($form, &$form_state) {
  // Verify that the url entered is actually for yr.no.
  $start = drupal_substr($form_state['values']['url'], 0, 17);
  $url = trim($form_state['values']['url']);
  // Make sure we always have a trailing slash.
  if (drupal_substr($url, -1) != '/') {
    $url .= '/';
  }
  // Figure out what language this is.
  $components = explode('/', drupal_substr($url, 17, -1));
  switch ($components[0]) {
    case 'place' : // English
      $lang = 'en';
      break;
    case 'sted' : // Norwegian Bokmål
      $lang = 'nb';
      break;
    case 'stad' : // Norwegian Nynorsk
      $lang = 'nn';
      break;
    case 'paikka' : // Kvääni
      $lang = 'no-kv';
      break;
    case 'sadji' : // Sami
      $lang = 'smi';
      break;
  }
  $n = count($components) - 1;
  $query = db_insert('yr_verdata')
    ->fields(array(
      'url' => $url,
      'lang' => $lang,
      'file' => md5($url) . '.xml',
      'weight' => $form_state['values']['weight'],
      'name' => str_replace('_', ' ', trim($components[$n])),
    ));
  if ($query->execute()) {
    drupal_set_message(t('Location added.'));
  }
}

/**
 * Function for deleting locations from the database table.
 *
 * @param $yid
 * The unique id of the location.
 * @return
 * Returns a confirmation form for deleting the given location.
 */
function yr_verdata_delete_confirm($form, &$form_state, $yid) {
  $form = array();
  $form['yid'] = array(
    '#type' => 'hidden',
    '#value' => $yid,
  );
  $location = _yr_verdata_load_location($yid, TRUE);
  _yr_verdata_filename($location);
  $form['filename'] = array(
    '#type' => 'hidden',
    '#value' => $location->actual_file,
  );
  $tmp = explode('/', $location->url);
  $num = count($tmp);
  $location->name = $tmp[$num-2];
  $question = t('Are you sure you want to remove the location %location, ID %yid from the database?', array('%location' => $location->name, '%yid' => (int) $yid));
  $description = t('The location and all related data will be removed from your site. You can re-add it later at !add.', array('!add' => l('forecast/add', 'forecast/add')));
  $yes = t('Remove');
  return confirm_form($form, $question, 'forecast', $description, $yes);
}

/**
 * Submit handler for the delete location form.
 */
function yr_verdata_delete_confirm_submit($form, &$form_state) {
  $query = db_delete('yr_verdata')
    ->condition('yid', $form_state['values']['yid'])
    ->execute();
  file_unmanaged_delete($form_state['values']['filename']);
  drupal_set_message(t('The location was deleted.'));
  drupal_goto('forecast');
}